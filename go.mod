module bitbucket.org/pcasmath/abelianmonoid

go 1.16

require (
	bitbucket.org/pcasmath/integer v0.0.1
	bitbucket.org/pcasmath/object v0.0.4
)
